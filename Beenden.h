//
//
//   Project Garagentorsteuerung
//!  \file Beenden.h
//!  \date 27. Apr. 2021
//!  \author Marcel Schneegans
//
//

#if !defined(_BEENDEN_H)
#define _BEENDEN_H

#include "Betriebszustand.h"

/*!
* Zustand Beenden im Zustandsautomaten des Betriebszustandes
*/
class Beenden : public Betriebszustand
{
public:
        virtual void handle();
        virtual void buttonInput(Garagentorsteuerung *pKontextKlasse, char pZeichen);
};

#endif //_BEENDEN_H
