//
//
//   Project Garagentorsteuerung
//!  \file Offen.h
//!  \date 26. Apr. 2021
//!  \author Felix Fakner
//
//

#if !defined(_OFFEN_H)
#define _OFFEN_H

#include "Torzustand.h"

/*!
* Zustand Offen im Zustandsautomaten des Torzustandes
*/
class Offen : public Torzustand
{
public:
        void handle(MotorSteuerung *pMotorSteuerung);
        void buttonInput(InBetrieb *pKontextKlasse, char pZeichen);
};

#endif //_OFFEN_H
