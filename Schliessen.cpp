//
//
//   Project Garagentorsteuerung
//!  \file Schliessen.cpp
//!  \date 26. Apr. 2021
//!  \author Felix Fakner
//
//

#include "Schliessen.h"
#include "Geschlossen.h"

/*!
* Ausfuehren der zustandsabhaengingen Aktionen
* \param pMotorSteuerung - Referenz zur Motorsteuerung
*/
void Schliessen::handle(MotorSteuerung *pMotorSteuerung)
{
  pMotorSteuerung->setDirection('r');
  pMotorSteuerung->start();
}

/*!
* Setzen des neuen Zustandes
* \param pKontextKlasse - Referenz auf die Kontextklasse
* \param pZeichen - Zeichen der Benutzereingabe
*/
void Schliessen::buttonInput(InBetrieb *pKontextKlasse, char pZeichen)
{
  if(pZeichen == 'F')
  {
    pKontextKlasse->changeState(new Geschlossen);
  }
}