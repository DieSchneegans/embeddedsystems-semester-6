//
//
//   Project Garagentorsteuerung
//!  \file gpio.cpp
//!  \date 19. Apr. 2021
//!  \author German Steile, Felix Fakner, Marcel Schneegans
//
//

#include "gpio.h"

// Konstruktor

/*!
* Konstruktor mit Parameterliste und Initialisierungsliste \n
* Erzeugung eines Objektes fuer Zugriff auf eine GPIO Bank des Mikrocontrollers
* \param startAdresse - Adresse des ModeRegisters des entsprechenden Ports
*/
Gpio::Gpio(uint32_t startAdresse) : rccAHB1enRegister(*(volatile uint32_t *)0x40023830),
                                    modeRegister(*(volatile uint32_t *)startAdresse),
                                    outputTypeRegister(*(volatile uint32_t *)(startAdresse + 0x0004)),
                                    outputSpeedRegister(*(volatile uint32_t *)(startAdresse + 0x0008)),
                                    pullUpPullDownRegister(*(volatile uint32_t *)(startAdresse + 0x000C)),
                                    inputDataRegister(*(volatile uint32_t *)(startAdresse + 0x0010)),
                                    outputDataRegister(*(volatile uint32_t *)(startAdresse + 0x0014)),
                                    bitSetResetRegisterLow(*(volatile uint16_t *)(startAdresse + 0x0018)),
                                    bitSetResetRegisterHigh(*(volatile uint16_t *)(startAdresse + 0x001A)),

                                    gpioBaseRegisterAdresse(0x40020000),
                                    registerDifference(0x00000400)
{
  enableGpioClock(); // Enable Clock register fuer Gpio Banks
}

/*!
* Enable clock register fuer die GPIO Bank \n
* Berechnung des Bits zur Aktivierung durch Basisadresse und Registerdifferenze
*/
void Gpio::enableGpioClock()
{
  uint32_t registerBit = ((uint32_t)&modeRegister - gpioBaseRegisterAdresse) / registerDifference;
  rccAHB1enRegister |= (1ul << registerBit);
}

// Pin Initialisierung

/*!
* Setzt im Mode Register einen Pin auf einen Modus
* \param pPin - GPIO Bank Pinnummer
* \param pMode - Modus des Pins
*/
void Gpio::setPinMode(uint32_t pPin, uint32_t pMode)
{
  modeRegister &= ~((3ul << 2 * pPin));
  modeRegister |= ((pMode << 2 * pPin));
}

/*!
* Setzt im Output Type Register einen Pin auf einen Modus
* \param pPin - GPIO Bank Pinnummer
* \param pMode - Modus des Pins
*/
void Gpio::setPinOutputType(uint32_t pPin, uint32_t pMode)
{
  outputTypeRegister &= ~((pMode << pPin));
}

/*!
* Setzt im Output Speed Register einen Pin auf einen Modus
* \param pPin - GPIO Bank Pinnummer
* \param pMode - Modus des Pins
*/
void Gpio::setPinOutputSpeed(uint32_t pPin, uint32_t pMode)
{
  outputSpeedRegister &= ~((3ul << 2 * pPin));
  outputSpeedRegister |= ((pMode << 2 * pPin));
}

/*!
* Setzt im Pull-Up/Pull-Down Register einen Pin auf einen Modus
* \param pPin - GPIO Bank Pinnummer
* \param pMode - Modus des Pins
*/
void Gpio::setPinPullUpPullDown(uint32_t pPin, uint32_t pMode)
{
  pullUpPullDownRegister &= ~((pMode << 2 * pPin));
}

// Output Methoden

/*!
* Setzt einen Pin als Ausgang
* \param pPin - GPIO Bank Pinnummer
*/
void Gpio::setPinOutput(uint32_t pPin)
{
  setPinMode(pPin, 1ul);
  setPinOutputType(pPin, 1ul);
  setPinOutputSpeed(pPin, 1ul);
  setPinPullUpPullDown(pPin, 3ul);
}

/*!
* Setzt den Datenwert eines Pins auf 1
* \param pPin - GPIO Bank Pinnummer
*/
void Gpio::setPinOn(uint32_t pPin)
{
  bitSetResetRegisterLow |= 1ul << pPin;
}

/*!
* Setzt den Datenwert eines Pins auf 0
* \param pPin - GPIO Bank Pinnummer
*/
void Gpio::setPinOff(uint32_t pPin)
{
  bitSetResetRegisterHigh |= 1ul << pPin;
}

/*!
* Setzt den Datenwert abh�ngig vom Datenwert auf 1 oder 0 (toggeln)
* \param pPin - GPIO Bank Pinnummer
*/
void Gpio::togglePin(uint32_t pPin)
{
  if ((outputDataRegister >> pPin) & 1ul)
  {
    setPinOff(pPin);
  }
  else
  {
    setPinOn(pPin);
  }
}

/*!
* Setzen eines Ports auf einen speziellen Datenwert
* \param pValue - Datenwerte fuer Register 
*/
void Gpio::setPort(uint32_t pValue)
{
  outputDataRegister = (portMask & pValue);
}

// Input Methoden

/*!
* Setzt einen Pin als Eingang
* \param pPin - GPIO Bank Pinnummer
*/
void Gpio::setPinInput(uint32_t pPin)
{
  setPinMode(pPin, 0ul);
  setPinPullUpPullDown(pPin, 3ul);
}

/*!
* Liest den Datenwert eines Pins ein
* \param pPin - GPIO Bank Pinnummer
* \return aktueller Wert des Pins (true - gedrueckt, false - nicht gedruckt)
*/
bool Gpio::readPin(uint32_t pPin)
{
  return (inputDataRegister >> pPin) & 1ul;
}

/*!
* Liest die Datenwerte einer Bank ein
* \return aktueller Wert des Ports
*/
uint32_t Gpio::readPort()
{
  return (inputDataRegister & portMask);
}
