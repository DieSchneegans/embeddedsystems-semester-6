//
//
//   Project Garagentorsteuerung
//!  \file Geschlossen.h
//!  \date 26. Apr. 2021
//!  \author Felix Fakner
//
//

#if !defined(_GESCHLOSSEN_H)
#define _GESCHLOSSEN_H

#include "Torzustand.h"

/*!
* Zustand Geschlossen im Zustandsautomaten des Torzustandes
*/
class Geschlossen : public Torzustand
{
public:
        void handle(MotorSteuerung *pMotorSteuerung);
        void buttonInput(InBetrieb *pKontextKlasse, char pZeichen);
};

#endif //_GESCHLOSSEN_H
