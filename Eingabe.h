//
//
//   Project Garagentorsteuerung
//!  \file Eingabe.h
//!  \date 21. Apr. 2021
//!  \author Automatisch generiert (WhiteStarUML)
//
//

#if !defined(_EINGABE_H)
#define _EINGABE_H

/*!
* Oberklasse zur Buendelung von Eingabemodulen
*/
class Eingabe
{
public:
	virtual char getTasterValue() = 0;
};

#endif //_EINGABE_H
