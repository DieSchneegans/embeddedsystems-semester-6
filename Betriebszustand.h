//
//
//   Project Garagentorsteuerung
//!  \file Betriebszustand.h
//!  \date 21. Apr. 2021
//!  \author Marcel Schneegans
//
//

#if !defined(_BETRIEBSZUSTAND_H)
#define _BETRIEBSZUSTAND_H

#include "Garagentorsteuerung.h"

#include "AusgabeKonsole.h"
#include "AusgabeBoard.h"

/*!
* Abstrakter Zustand fuer das State Pattern "Garagentorsteuerung"
*/
class Betriebszustand
{
protected:
        //! Referenz zur Motorsteuerung
        MotorSteuerung *motorSteuerung;

public:
        Betriebszustand();
        virtual ~Betriebszustand();
        virtual void handle() = 0;
        virtual void buttonInput(Garagentorsteuerung *pKontextKlasse, char pZeichen) = 0;
};

#endif //_BETRIEBSZUSTAND_H