//
//
//   Project Garagentorsteuerung
//!  \file Torstand.h
//!  \date 26. Apr. 2021
//!  \author Automatisch generiert (WhiteStarUML)
//
//

#if !defined(_TORZUSTAND_H)
#define _TORZUSTAND_H

#include "InBetrieb.h"

/*!
* Abstrakter Zustand fuer das State Pattern "Torzustnad"
*/
class Torzustand
{
public:
        virtual void handle(MotorSteuerung *pMotorSteuerung) = 0;
        virtual void buttonInput(InBetrieb *pKontextKlasse, char pZeichen) = 0;
};

#endif //_TORZUSTAND_H
