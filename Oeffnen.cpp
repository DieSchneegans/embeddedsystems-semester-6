//
//
//   Project Garagentorsteuerung
//!  \file Oeffnen.cpp
//!  \date 26. Apr. 2021
//!  \author Felix Fakner
//
//

#include "Oeffnen.h"
#include "Offen.h"

/*!
* Ausfuehren der zustandsabhaengingen Aktionen
* \param pMotorSteuerung - Referenz zur Motorsteuerung
*/
void Oeffnen::handle(MotorSteuerung *pMotorSteuerung)
{
  pMotorSteuerung->setDirection('l');
  pMotorSteuerung->start();
}

/*!
* Setzen des neuen Zustandes
* \param pKontextKlasse - Referenz auf die Kontextklasse
* \param pZeichen - Zeichen der Benutzereingabe
*/
void Oeffnen::buttonInput(InBetrieb *pKontextKlasse, char pZeichen)
{
  if(pZeichen == 'F')
  {
    pKontextKlasse->changeState(new Offen);
  }
}