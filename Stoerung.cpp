//
//
//   Project Garagentorsteuerung
//!  \file Stoerung.cpp
//!  \date 26. Apr. 2021
//!  \author German Steile
//
//

#include "Stoerung.h"
#include "InBetrieb.h"

/*!
* Ausfuehren der zustandsabhaengingen Aktionen
*/
void Stoerung::handle()
{
  motorSteuerung->setCurrentless();
}

/*!
* Setzen des neuen Zustandes
* \param pKontextKlasse - Referenz auf die Kontextklasse
* \param pZeichen - Zeichen der Benutzereingabe
*/
void Stoerung::buttonInput(Garagentorsteuerung *pKontextKlasse, char pZeichen)
{
  if (pZeichen == '0')
  {
    handle();
  }
  if (pZeichen == 'B')
  {
    pKontextKlasse->changeState(new InBetrieb);
  }
}