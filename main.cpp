//
//
//   Project Garagentorsteuerung
//!  \file main.cpp
//!  \date 27. Apr. 2021
//!  \author German Steile
//
//

#include "Garagentorsteuerung.h"

int main()
{
  Garagentorsteuerung steuerung;
  steuerung.startApplication();

  return 0;
}