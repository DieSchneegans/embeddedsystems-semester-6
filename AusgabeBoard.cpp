//
//
//   Project Garagentorsteuerung
//!  \file AusgabeBoard.cpp
//!  \date 21. Apr. 2021
//!  \author German Steile
//
//

#include "AusgabeBoard.h"

/*!
* Konstruktor mit Initialisierungsliste \n
* mit Festleung der Motordrehrichtung
*/
AusgabeBoard::AusgabeBoard() : leds(), blinken(0)
{
  direction = 'r';
}

/*!
* Setzen der Motordrehrichtung
* \param pDirection - Drehrichtungsvorgabe ('r' - rechts, 'l' - links)
*/
inline void AusgabeBoard::setDirection(char pDirection)
{
  direction = pDirection;
}

/*!
* Starten des Motors
*/
void AusgabeBoard::start()
{
  if (direction == 'l')
  {
    leds.setLeds(0x6); // Garagentor oeffnend -> ledGelb & ledGruen ON
  }
  else if (direction == 'r')
  {
    leds.setLeds(0x3); // Garagentor schliessend -> ledGelb & ledRot ON
  }
  else
  {
    leds.setLeds(0x7); // undefinierter Zustand ERROR -> Alle LEDs ON
  }
}

/*!
* Stoppen des Motors
*/
void AusgabeBoard::stop()
{
  if (direction == 'l')
  {
    leds.setLeds(0x4); // Garagentor offen -> ledGruen ON
  }
  else if (direction == 'r')
  {
    leds.setLeds(0x1); // Garagentor geschlossen -> ledRot ON
  }
  else
  {
    leds.setLeds(0x7); // undefinierter Zustand ERROR -> Alle LEDs ON
  }
}

/*!
* STO - SafeTorqueOff \n
* Freischalten des Motors
*/
void AusgabeBoard::setCurrentless()
{
  leds.setLeds(0x0); // Garagentor in Stoerzustand -> Alle LEDs OFF

  if (blinken) // Gelbe LED blinken lassen
  {
    leds.setLed('y', 1);
    blinken = 0;
  }
  else
  {
    leds.setLed('y', 0);
    blinken = 1;
  }
}

/*!
* Motor ausschalten
*/
void AusgabeBoard::deactivate()
{
  leds.setLeds(0x0); // Alle LEDs OFF
}
