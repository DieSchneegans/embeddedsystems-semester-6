//
//
//   Project Garagentorsteuerung
//!  \file Oeffnen.h
//!  \date 26. Apr. 2021
//!  \author Felix Fakner
//
//

#if !defined(_OEFFNEN_H)
#define _OEFFNEN_H

#include "Torzustand.h"

/*!
* Zustand Oeffnen im Zustandsautomaten des Torzustandes
*/

class Oeffnen : public Torzustand
{
public:
        void handle(MotorSteuerung *pMotorSteuerung);
        void buttonInput(InBetrieb *pKontextKlasse, char pZeichen);
};

#endif //_OEFFNEN_H
