//
//
//   Project Garagentorsteuerung
//!  \file UserLeds.h
//!  \date 21. Apr. 2021
//!  \author German Steile, Felix Fakner, Marcel Schneegans
//
//

#if !defined(_USERLEDS_H)
#define _USERLEDS_H

#include "gpio.h"

/*!
* Verwalten der zur Verfuegung stehenden LEDs
*/
class UserLeds
{
private:
        //! Pin-Nr der roten LED
        int ledRot;
        //! Pin-Nr der gelben LED
        int ledGelb;
        //! Pin-Nr der gruenen LED
        int ledGruen;
        //! Referenz zur GPIO Bank
        Gpio gpio;

        void init();

public:
        UserLeds();
        void setLed(char pColor, bool pValue);
        void setLeds(unsigned int pValue);
};

#endif //_USERLEDS_H
