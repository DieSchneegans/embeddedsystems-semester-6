//
//
//   Project Garagentorsteuerung
//!  \file EingabeKonsole.h
//!  \date 21. Apr. 2021
//!  \author Felix Fakner
//
//

#if !defined(_EINGABEKONSOLE_H)
#define _EINGABEKONSOLE_H

#include "Eingabe.h"
#include <iostream>
using namespace std;

/*!
* Simulieren einer Tastereingabe ueber die Konsole
*/
class EingabeKonsole : public Eingabe
{
public:
        char getTasterValue();
};

#endif //_EINGABEKONSOLE_H
