//
//
//   Project Garagentorsteuerung
//!  \file InBetrieb.cpp
//!  \date 26. Apr. 2021
//!  \author German Steile
//
//

#include "InBetrieb.h"
#include "Stoerung.h"
#include "Geschlossen.h"

/*!
* Konstruktor mit Erzeugung des Zustandsautomatens \n
* Default-Zustand - Geschlossen
*/
InBetrieb::InBetrieb()
{
  // Default-Zustand: Geschlossen
  zustand = new Geschlossen();
}

/*!
* Destruktor zum loeschen des Zustandsautomaten
*/
InBetrieb::~InBetrieb()
{
  delete zustand;
}

/*!
* Setzen des naechsten Zustandes im Zustandsautomaten
* \param pNextState - Referenz auf den naechsten Zustand
*/
void InBetrieb::changeState(Torzustand *pNextState)
{
  delete zustand;
  zustand = pNextState;
  zustand->handle(motorSteuerung);
}

/*!
* Ausfuehren der zustandsabhaengingen Aktionen
*/
void InBetrieb::handle()
{
  zustand->handle(motorSteuerung);
}

/*!
* Eingabe des Benutzers am Zustand weitergeben oder Zustand wechseln
* \param pZeichen - Eingegebenes Zeichen
* \param pKontextKlasse - Referenz auf die Kontextklasse
*/
void InBetrieb::buttonInput(Garagentorsteuerung *pKontextKlasse, char pZeichen)
{
  if (pZeichen == 'B')
  {
    pKontextKlasse->changeState(new Stoerung);
  }
  else
  {
    zustand->buttonInput(this, pZeichen);
  }
}
