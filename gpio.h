//
//
//   Project Garagentorsteuerung
//!  \file gpio.cpp
//!  \date 19. Apr. 2021
//!  \author German Steile, Felix Fakner, Marcel Schneegans
//
//

#ifndef _GPIO_
#define _GPIO_

#include "stdint.h"

/*!
* Klasse zum Zugreifen auf die GPIOs des Nucleo Boards
*/
class Gpio
{
private:
  // Deklaration des Gpio Clock Registers
  //! RCC AHB1 peripheral clock enable register(RCC_AHB1ENR)
  volatile uint32_t &rccAHB1enRegister;

  // Deklaration der Gpio Port Register
  //! GPIO port mode register
  volatile uint32_t &modeRegister;
  //! GPIO port output type register
  volatile uint32_t &outputTypeRegister;
  //! GPIO port output speed register
  volatile uint32_t &outputSpeedRegister;
  //! GPIO port pull-up/pull-down register
  volatile uint32_t &pullUpPullDownRegister;
  //! GPIO port input data register
  const volatile uint32_t &inputDataRegister;
  //! GPIO port output data register
  volatile uint32_t &outputDataRegister;
  //! GPIO port bit set/reset register (low)
  volatile uint16_t &bitSetResetRegisterLow;
  //! GPIO port bit set/reset register (high)
  volatile uint16_t &bitSetResetRegisterHigh;

  // Merker zur Berechnung des Clock Enable
  //! Merker fuer die Basisadresse der Gpio Port Register
  const uint32_t gpioBaseRegisterAdresse;
  //! Merker fuer die Differenze zwischen den Gpio Ports
  const uint32_t registerDifference;

  //! Maske fuer reservierte Bits (16..32 => 0)
  const uint32_t portMask = 0xFFFF;

  // Methoden
  void enableGpioClock();
  void setPinMode(uint32_t pPin, uint32_t pMode);
  void setPinOutputType(uint32_t pPin, uint32_t pMode = 1);
  void setPinOutputSpeed(uint32_t pPin, uint32_t pMode = 0);
  void setPinPullUpPullDown(uint32_t pPin, uint32_t pMode = 0);

public:
  // Konstruktor
  Gpio(uint32_t startAdresse);

  // Output Methoden
  void setPinOutput(uint32_t pPin);
  void setPinOff(uint32_t pPin);
  void setPinOn(uint32_t pPin);
  void togglePin(uint32_t pPin);
  void setPort(uint32_t pValue);

  // Input Methoden
  void setPinInput(uint32_t pPin);
  bool readPin(uint32_t pPin);
  uint32_t readPort();
};

#endif