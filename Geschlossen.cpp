//
//
//   Project Garagentorsteuerung
//!  \file Geschlossen.cpp
//!  \date 26. Apr. 2021
//!  \author Felix Fakner
//
//

#include "Geschlossen.h"
#include "Oeffnen.h"

/*!
* Ausfuehren der zustandsabhaengingen Aktionen
* \param pMotorSteuerung - Referenz zur Motorsteuerung
*/
void Geschlossen::handle(MotorSteuerung *pMotorSteuerung)
{
  pMotorSteuerung->stop();
}

/*!
* Setzen des neuen Zustandes
* \param pKontextKlasse - Referenz auf die Kontextklasse
* \param pZeichen - Zeichen der Benutzereingabe
*/
void Geschlossen::buttonInput(InBetrieb *pKontextKlasse, char pZeichen)
{
  if(pZeichen == 'F')
  {
    pKontextKlasse->changeState(new Oeffnen);
  }
}
