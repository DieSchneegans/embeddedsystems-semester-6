//
//
//   Project Garagentorsteuerung
//!  \file Offen.cpp
//!  \date 26. Apr. 2021
//!  \author Felix Fakner
//
//

#include "Offen.h"
#include "Schliessen.h"

/*!
* Ausfuehren der zustandsabhaengingen Aktionen
* \param pMotorSteuerung - Referenz zur Motorsteuerung
*/
void Offen::handle(MotorSteuerung *pMotorSteuerung)
{
  pMotorSteuerung->stop();
}

/*!
* Setzen des neuen Zustandes
* \param pKontextKlasse - Referenz auf die Kontextklasse
* \param pZeichen - Zeichen der Benutzereingabe
*/
void Offen::buttonInput(InBetrieb *pKontextKlasse, char pZeichen)
{
  if(pZeichen == 'F')
  {
    pKontextKlasse->changeState(new Schliessen);
  }
}
