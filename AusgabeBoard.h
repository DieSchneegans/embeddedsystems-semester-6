//
//
//   Project Garagentorsteuerung
//!  \file AusgabeBoard.h
//!  \date 21. Apr. 2021
//!  \author German Steile
//
//

#if !defined(_AUSGABEBOARD_H)
#define _AUSGABEBOARD_H

#include "MotorSteuerung.h"
#include "UserLeds.h"

/*!
* Erzeugung von Ledausgaben zur Darstellung einer Motorsteuerung
*/
class AusgabeBoard : public MotorSteuerung
{
private:
        //! Referenz zu UserLeds
        UserLeds leds;

        //! Zustand der Stoerungssignalisierung
        bool blinken;

public:
        AusgabeBoard();
        void setDirection(char pDirection);
        void start();
        void stop();
        void setCurrentless();
        void deactivate();
};

#endif //_AUSGABEBOARD_H