//
//
//   Project Garagentorsteuerung
//!  \file Betriebszustand.cpp
//!  \date 21. Apr. 2021
//!  \author Marcel Schneegans
//
//

#include "Betriebszustand.h"

/*!
* Konstruktor mit Erzeugung der Motorsteuerung (In Abhaengigkeit der Hardware)
*/
Betriebszustand::Betriebszustand()
{
#ifdef HARDWARE
  motorSteuerung = new AusgabeBoard();
#else
  motorSteuerung = new AusgabeKonsole();
#endif
}

/*!
* Destruktor zum Loeschen der Motorsteuerung
*/
Betriebszustand::~Betriebszustand()
{
  delete motorSteuerung;
}