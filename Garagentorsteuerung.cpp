//
//
//   Project Garagentorsteuerung
//!  \file Garagentorsteuerung.cpp
//!  \date 21. Apr. 2021
//!  \author Marcel Schneegans
//
//

#include "Garagentorsteuerung.h"
#include "InBetrieb.h"
#include "Beenden.h"

/*!
* Konstruktor mit Erzeugung des Zustandsautomatens \n
* Default-Zustand - InBetrieb
*/
Garagentorsteuerung::Garagentorsteuerung()
{
  // Default-Zustand: InBetrieb
  zustand = new InBetrieb();
}

/*!
* Destruktor zum Loeschen des Zustandsautomaten
*/
Garagentorsteuerung::~Garagentorsteuerung()
{
  delete zustand;
}

/*!
* Starten der Applikation "Garagentorsteuerung"
*/
void Garagentorsteuerung::startApplication()
{
#ifdef HARDWARE
  Eingabe *eingabe = new UserButtons();
#else
  Eingabe *eingabe = new EingabeKonsole();
#endif
  zustand->handle();
  while (true)
  {
    char input = eingabe->getTasterValue();
    if (input == 'X')
      break;
    buttonInput(input);
  }
  changeState(new Beenden);
}

/*!
* Setzen des naechsten Zustandes im Zustandsautomaten
* \param pNextState - Referenz auf den naechsten Zustand
*/
void Garagentorsteuerung::changeState(Betriebszustand *pNextState)
{
  delete zustand;
  zustand = pNextState;
  zustand->handle();
}

/*!
* Eingabe des Benutzers am Zustand weitergeben 
* \param pZeichen - Eingegebenes Zeichen
*/
void Garagentorsteuerung::buttonInput(char pZeichen)
{
  zustand->buttonInput(this, pZeichen);
}
