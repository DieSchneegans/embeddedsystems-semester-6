//
//
//   Project Garagentorsteuerung
//!  \file Geschlossen.cpp
//!  \date 27. Apr. 2021
//!  \author Marcel Schneegans
//
//

#include "Beenden.h"

/*!
* Ausfuehren der zustandsabhaengingen Aktionen
*/
void Beenden::handle()
{
  motorSteuerung->deactivate();
}

/*!
* Setzen des neuen Zustandes
* \param pKontextKlasse - Referenz auf die Kontextklasse
* \param pZeichen - Zeichen der Benutzereingabe
*/
inline void Beenden::buttonInput(Garagentorsteuerung *pKontextKlasse, char pZeichen)
{
}
