//
//
//   Project Garagentorsteuerung
//!  \file EingabeKonsole.cpp
//!  \date 21. Apr. 2021
//!  \author Felix Fakner
//
//

#include "EingabeKonsole.h"

/*!
* Einlesen eines Tasterdrucks ueber die Konsole (Tastaturdruck)
* \return Wert der Tastaturbuffers
*/
char EingabeKonsole::getTasterValue()
{
  char input;
  cin >> input;
  return input;
}
