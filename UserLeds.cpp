//
//
//   Project Garagentorsteuerung
//!  \file UserLeds.cpp
//!  \date 21. Apr. 2021
//!  \author German Steile, Felix Fakner, Marcel Schneegans
//
//

#include "UserLeds.h"

/*!
* Konstruktor mit Initalisierungsliste zur Festlegung der LEDs
*/
UserLeds::UserLeds() : ledRot(8), ledGelb(9), ledGruen(10), gpio(0x40020000)
{
  init();
}

/*!
* Initialisieren der LEDs als Ausgang
*/
void UserLeds::init()
{
  gpio.setPinOutput(ledRot);
  gpio.setPinOutput(ledGelb);
  gpio.setPinOutput(ledGruen);
}

/*!
* Setzen des Wertes einer LED
* \param pColor - Auswahl der LED ('r' - rote LED, 'y' - gelbe LED, 'g' - gruene LED)
* \param pValue - Wert der LED (true - einschalten, false - ausschalten)
*/
void UserLeds::setLed(char pColor, bool pValue)
{
  int gpioPin = -1;

  switch (pColor)
  {
  case 'r':
    gpioPin = ledRot;
    break;
  case 'y':
    gpioPin = ledGelb;
    break;
  case 'g':
    gpioPin = ledGruen;
    break;
  }

  if (gpioPin != -1)
  {
    if (pValue)
    {
      gpio.setPinOn(gpioPin);
    }
    else
    {
      gpio.setPinOff(gpioPin);
    }
  }
}

/*!
* Setzen der verfuegbaren LEDs
* \param pValue - zu schreibender Wert fuer die LEDs (in Hex)
*/
void UserLeds::setLeds(unsigned int pValue)
{
  if (pValue > 0x7)
  {
    pValue = 0x7;
  }
  gpio.setPort(pValue << ledRot);
}
