var annotated_dup =
[
    [ "AusgabeBoard", "class_ausgabe_board.html", "class_ausgabe_board" ],
    [ "AusgabeKonsole", "class_ausgabe_konsole.html", "class_ausgabe_konsole" ],
    [ "Beenden", "class_beenden.html", "class_beenden" ],
    [ "Betriebszustand", "class_betriebszustand.html", "class_betriebszustand" ],
    [ "Eingabe", "class_eingabe.html", "class_eingabe" ],
    [ "EingabeKonsole", "class_eingabe_konsole.html", "class_eingabe_konsole" ],
    [ "Garagentorsteuerung", "class_garagentorsteuerung.html", "class_garagentorsteuerung" ],
    [ "Geschlossen", "class_geschlossen.html", "class_geschlossen" ],
    [ "Gpio", "class_gpio.html", "class_gpio" ],
    [ "InBetrieb", "class_in_betrieb.html", "class_in_betrieb" ],
    [ "MotorSteuerung", "class_motor_steuerung.html", "class_motor_steuerung" ],
    [ "Oeffnen", "class_oeffnen.html", "class_oeffnen" ],
    [ "Offen", "class_offen.html", "class_offen" ],
    [ "Schliessen", "class_schliessen.html", "class_schliessen" ],
    [ "Stoerung", "class_stoerung.html", "class_stoerung" ],
    [ "Torzustand", "class_torzustand.html", "class_torzustand" ],
    [ "UserButtons", "class_user_buttons.html", "class_user_buttons" ],
    [ "UserLeds", "class_user_leds.html", "class_user_leds" ]
];