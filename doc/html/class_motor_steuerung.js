var class_motor_steuerung =
[
    [ "deactivate", "class_motor_steuerung.html#ab4efcdfd706b7a46de615ebfde2ce0f3", null ],
    [ "setCurrentless", "class_motor_steuerung.html#a23da9f0cd18485eaa421e88196db38c5", null ],
    [ "setDirection", "class_motor_steuerung.html#ab361681c827c9f85ff3f76d08497db34", null ],
    [ "start", "class_motor_steuerung.html#a4050e9ff8ad5bc38bc27b08c16c0e3f6", null ],
    [ "stop", "class_motor_steuerung.html#a828b0cfb476ce582625409850f233c9b", null ],
    [ "direction", "class_motor_steuerung.html#a0be4fcd76fd465f4abb70823607ffe98", null ]
];