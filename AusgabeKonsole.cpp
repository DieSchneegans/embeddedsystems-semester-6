//
//
//   Project Garagentorsteuerung
//!  \file AusgabeKonsole.cpp
//!  \date 21. Apr. 2021
//!  \author Marcel Schneegans
//
//

#include "AusgabeKonsole.h"

/*!
* Konstruktor zur Erzeugung eines Objektes zur Ausgabe der Motorsteuerung auf der Konsole
*/
AusgabeKonsole::AusgabeKonsole()
{
  direction = 'r';
}

/*!
* Setzen der Drehrichtung des Motors (als Konsolenausgabe)
* \param pDirection - Drehrichtungsvorgabe ('r' - rechts, 'l' - links)
*/
void AusgabeKonsole::setDirection(char pDirection)
{
  direction = pDirection;
  cout << "setDirection, " << pDirection << endl;
}

/*!
* Starten des Motors (als Konsolenausgabe)
*/
void AusgabeKonsole::start()
{
  cout << "setStart";

  if (direction == 'r')
  {
    cout << ", Bewegung nach unten!" << endl;
  }
  if (direction == 'l')
  {
    cout << ", Bewegung nach oben!" << endl;
  }
}

/*!
* Stoppen des Motors (als Konsolenausgabe)
*/
inline void AusgabeKonsole::stop()
{
  cout << "setStop" << endl;
}

/*!
* Freischalten des Motors (als Konsolenausgabe)
*/
inline void AusgabeKonsole::setCurrentless()
{
  cout << "setCurrentless, Stoerung" << endl;
}

/*!
* Motor ausschalten
*/
inline void AusgabeKonsole::deactivate()
{
  cout << "Motor wurde deaktiviert" << endl;
}