//
//
//   Project Garagentorsteuerung
//!  \file UserButtons.h
//!  \date 21. Apr. 2021
//!  \author Felix Fakner
//
//

#if !defined(_USERBUTTONS_H)
#define _USERBUTTONS_H

#include "Eingabe.h"
#include "Gpio.h"

/*!
*Verwaltung der zur Verfuegung stehenden Taster
*/
class UserButtons : public Eingabe
{
private:
        //! Pin-Nr des Tasters auf Steckplatz D3 (Taster1)
        int buttonD3;
        //! Pin-Nr des Tasters auf Steckplatz D4 (Taster2)
        int buttonD4;
        //! Zustand Taster1 zur Flankenerkennung
        unsigned char zustandButtonD3;
        //! Zustand Taster2 zur Flankenerkennung
        unsigned char zustandButtonD4;

        //! Referenz zur GPIO-Bank
        Gpio gpio;

        void init();
        int entprelleTaster(int pTaster, unsigned char &pZustand);

public:
        UserButtons();
        char getTasterValue();
};

#endif //_USERBUTTONS_H
