//
//
//   Project Garagentorsteuerung
//!  \file InBetrieb.h
//!  \date 26. Apr. 2021
//!  \author German Steile
//
//

#if !defined(_INBETRIEB_H)
#define _INBETRIEB_H

#include "Betriebszustand.h"

class Torzustand;

/*!
* Zustand des Zustandsautomaten Betriebszustand \n
* Kontextklasse des Zustandsautomaten Torzustand
*/
class InBetrieb : public Betriebszustand
{
private:
        //! Referenz zum Zustandsautomat
        Torzustand *zustand;

public:
        InBetrieb();
        virtual ~InBetrieb();
        void changeState(Torzustand *pNextState);
        void handle();
        virtual void buttonInput(Garagentorsteuerung *pKontextKlasse, char pZeichen);
};
#endif //_INBETRIEB_H