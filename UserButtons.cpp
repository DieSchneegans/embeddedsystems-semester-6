//
//
//   Project Garagentorsteuerung
//!  \file UserButtons.cpp
//!  \date 21. Apr. 2021
//!  \author Felix Fakner
//
//

#include "UserButtons.h"
#include <iostream>
using namespace std;

/*!
* Konstruktor mit Initalisierungsliste zur Festlegung der Buttons
*/
UserButtons::UserButtons() : buttonD3(3), buttonD4(5), zustandButtonD3(1), zustandButtonD4(1), gpio(0x40020400)
{
  init();
}

/*!
* Initialisieren der Buttons als Eingang
*/
void UserButtons::init()
{
  gpio.setPinInput(buttonD3);
  gpio.setPinInput(buttonD4);
}

/*!
* Allgemeine Flankenabfrage eines uebergebenen Tasters
* Quelle, Stand 21.04.2021: https://www.mikrocontroller.net/articles/Entprellung#Softwareentprellung
* \param pTaster - zu untersuchender Taster
* \param pZustand - Pointer auf die Zustandsvariable zur Flankendetektierung
*/
int UserButtons::entprelleTaster(int pTaster, unsigned char &pZustand)
{
  int rw = 0;

  if (pZustand == 0 && !(gpio.readPort() & (1 << pTaster))) //Taster wird gedrueckt (steigende Flanke
  {
    pZustand = 1;
    rw = 1;
  }
  else if (((pZustand == 1) || (pZustand == 2)) && !(gpio.readPort() & (1 << pTaster))) //Taster wird gehalten
  {
    pZustand = 2;
    rw = 0;
  }
  else if (pZustand == 2 && (gpio.readPort() & (1 << pTaster))) //Taster wird losgelassen
  {
    pZustand = 3;
    rw = 0;
  }
  else if (pZustand == 3 && (gpio.readPort() & (1 << pTaster))) //Taster losgelassen
  {
    pZustand = 0;
    rw = 0;
  }

  return rw;
}

/*!
* Abfragen des Eingabestatus
* \return auswahl as char - 'B' = buttonD3 (Taster1) gedrueckt, 'F' = buttonD4 (Taster2) gedrueckt 'X' = Beide Taster, '0' = Kein Taster
*/
char UserButtons::getTasterValue()
{
  //Daten Auslesen
  int buttonD3_isPressed = 0;
  int buttonD4_isPressed = 0;
  int auswahl = 0;

  //Eingabezeit realisieren fuer Fall Beide Taster werden betaetigen
  for (int i = 1; i < 10000; i++)
  {
    //Abfrage der TasterFlanken
    if (entprelleTaster(buttonD3, zustandButtonD3))
    {
      buttonD3_isPressed = 1;
    }
    if (entprelleTaster(buttonD4, zustandButtonD4))
    {
      buttonD4_isPressed = 1;
    }
    //Caseauswahl berechnen
    auswahl = buttonD3_isPressed + buttonD4_isPressed * 2;
    //sobald beide Taster gedrueckt wurden ist zusaetzliche Zeit irrelevant
    if (auswahl == 3)
    {
      break;
    }
  }

  // Auswertung Rueckgabewert
  switch (auswahl)
  {
  case (1): // PB3 betaetigt
    return 'B';
    break;
  case (2): // PB5 betaetigt
    return 'F';
    break;
  case (3): // Beide Taster
    return 'X';
    break;
  default: // kein Taster
    return '0';
    break;
  }
}
