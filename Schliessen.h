//
//
//   Project Garagentorsteuerung
//!  \file Schliessen.h
//!  \date 26. Apr. 2021
//!  \author Felix Fakner
//
//

#if !defined(_SCHLIESSEN_H)
#define _SCHLIESSEN_H

#include "Torzustand.h"

/*!
* Zustand Schliessen im Zustandsautomaten des Torzustandes
*/
class Schliessen : public Torzustand
{
public:
        void handle(MotorSteuerung *pMotorSteuerung);
        void buttonInput(InBetrieb *pKontextKlasse, char pZeichen);
};

#endif //_SCHLIESSEN_H
