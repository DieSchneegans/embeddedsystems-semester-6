//
//
//   Project Garagentorsteuerung
//!  \file Stoerung.h
//!  \date 26. Apr. 2021
//!  \author German Steile
//
//

#if !defined(_STOERUNG_H)
#define _STOERUNG_H

#include "Betriebszustand.h"

/*!
* Zustand Stoerung im Zustandsautomaten des Betriebszustandes
*/
class Stoerung : public Betriebszustand
{
public:
        virtual void handle();
        virtual void buttonInput(Garagentorsteuerung *pKontextKlasse, char pZeichen);
};

#endif //_STOERUNG_H
