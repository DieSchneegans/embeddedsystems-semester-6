//
//
//   Project Garagentorsteuerung
//!  \file Motorsteuerung.h
//!  \date 26. Apr. 2021
//!  \author Automatisch generiert (WhiteStarUML)
//
//

#if !defined(_MOTORSTEUERUNG_H)
#define _MOTORSTEUERUNG_H

/*!
* Interface-Klasse zum Festlegen von Motorsteuerungen
*/
class MotorSteuerung
{
protected:
	//! Drehrichtung des Motors
	char direction;

public:
	virtual void setDirection(char pDirection) = 0;
	virtual void start() = 0;
	virtual void stop() = 0;
	virtual void setCurrentless() = 0;
	virtual void deactivate() = 0;
};

#endif //_MOTORSTEUERUNG_H
