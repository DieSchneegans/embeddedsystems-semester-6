//
//
//   Project Garagentorsteuerung
//!  \file AusgabeKonsole.h
//!  \date 21. Apr. 2021
//!  \author Marcel Schneegans
//
//

#if !defined(_AUSGABEKONSOLE_H)
#define _AUSGABEKONSOLE_H

#include "MotorSteuerung.h"
#include "iostream"
using namespace std;

/*!
* Erzeugung von Konsolenausgaben zur Darstellung einer Motorsteuerung
*/
class AusgabeKonsole : public MotorSteuerung
{
public:
	AusgabeKonsole();
	void setDirection(char pDirection);
	void start();
	void stop();
	void setCurrentless();
	void deactivate();
};

#endif //_AUSGABEKONSOLE_H
