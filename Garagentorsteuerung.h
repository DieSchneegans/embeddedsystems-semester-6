//
//
//   Project Garagentorsteuerung
//!  \file Garagentorsteuerung.h
//!  \date 21. Apr. 2021
//!  \author Marcel Schneegans
//
//

#ifndef _GARAGENTORSTEUERUNG_H
#define _GARAGENTORSTEUERUNG_H
// Anlegen der Klasse - sonst nicht compilierbar
class Betriebszustand;

#include "EingabeKonsole.h"
#include "UserButtons.h"

/*!
* Zentrale Steuerungseinheit
*/
class Garagentorsteuerung
{
private:
        //! Referenz zum Zustandsautomat
        Betriebszustand *zustand;

public:
        Garagentorsteuerung();
        ~Garagentorsteuerung();
        void startApplication();
        void changeState(Betriebszustand *pNextState);
        void buttonInput(char pZeichen);
};

#endif //_GARAGENTORSTEUERUNG_H
